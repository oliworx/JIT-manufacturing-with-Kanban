Implementation of Just-in-time production with Kanban
=====================================================

project paper / Seminararbeit: _Implementierung der Just-in-time-Produktion mittels Kanban_

Date: 2015-05-15

Language: German

Download PDF: https://github.com/oliworx/JIT-manufacturing-with-Kanban/raw/master/kanban.pdf

Mit der zunehmenden Globalisierung steht heute praktisch jedes produzierende Un-
ternehmen im internationalen Wettbewerb. Der Markt fordert hohe Flexibilität und
Lieferfähigkeit, wobei die Kosten immer weiter sinken sollen, um wettbewerbsfähig
zu bleiben. Die japanische Firma Toyota hat diese Problematik für sich bereits in
den 1950er Jahren erkannt und eigene Lösungen dafür gesucht. So ist schliesslich im
Laufe der folgendenden Jahrzehnte das Toyota-Produktionssystem (TPS) entwickelt
worden, dessen wesentliche Bestandteile heute auch unter den Begriffen Just-in-
Time oder Lean-Production bekannt sind. Ein wichtiger Bestandteil des TPS ist das
Kanban-System, das vor allem von Taiichi Ohno entwickelt wurde.

Die vorliegende Arbeit gibt eine Einführung in die wesentlichen Methoden, Werk-
zeuge und Prinzipien des Kanban-Systems und wie mit Hilfe von Kanban eine Just-
in-Time-Produktion umgesetzt werden kann. Im zweiten Abschnitt wird hierzu die
Geschichte und Entwicklung von Kanban vorgestellt. Im Abschnitt drei wird auf-
gezeigt, wie Kanban im produzierenden Betrieb eingeführt werden kann und wie
dadurch die Wettbewerbsfähigkeit gesteigert wird. Eng in Verbindung mit Kanban
steht der kontinuierliche Verbesserungsprozess (KVP), auch Kaizen genannt, was im
vierten Abschnitt betrachtet wird. Im letzten Abschnitt wird kritisch auf die Risi-
ken einer Kanban-Einführung eingegangen und ein Ausblick auf die zu erwartende
künftige Entwicklung gegeben.

Für die Erstellung dieser Arbeit wurde auf relevante Fachliteratur zurückgegriffen
und teilweise auch im Internet recherchiert.
